import React , { Component } from "react";
import { Switch, Route, Redirect } from "react-router-dom";

// components

import AdminNavbar from "components/Navbars/AdminNavbar.js";
import Sidebar from "components/Sidebar/Sidebar.js";
import FooterAdmin from "components/Footers/FooterAdmin.js";

// views

import Dashboard from "components/Admin/Dashboard.js";

// Roles
import Roles from "components/Admin/Roles/List.js";
import RolesForm from "components/Admin/Roles/Form.js";
import RolesEdit from "components/Admin/Roles/Edit.js";


export default function Admin() {
  return (
    <>
      <Sidebar />
      <div className="relative md:ml-64 bg-blueGray-100">
        <AdminNavbar />
        <div className="px-4 md:px-10 mx-auto w-full -m-24">
          <Switch>
            <Route path="/admin/dashboard" exact component={Dashboard} />
            <Route path="/admin/roles" exact component={Roles} />
            <Route path="/admin/roles/form" exact component={RolesForm} />
            <Route path="/admin/roles/edit/:id" component={RolesEdit} />
            <Redirect from="/" to="/admin/dashboard" />
          </Switch>
          <FooterAdmin />
        </div>
      </div>
    </>
  );
}
