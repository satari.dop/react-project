import React , { useEffect, useState } from "react";
import { Link } from "react-router-dom";
import { confirmAlert } from 'react-confirm-alert'; // Import
import 'react-confirm-alert/src/react-confirm-alert.css'; // Import css

// services
import roleServices from "../../../services/Admin/Role"


export default function Form() {

  const [ rol_name, setName ] = useState(null);

  const saveRole = async () => {

    const data = {
        rol_name
    }
    const res = await roleServices.save(data);
    if (res.success) {
      alert(res.message)
    }
    else {
      alert(res.message)
    }
  }
  return (
    <>
    <div className="flex flex-wrap">
        <div className="w-full lg:w-12/12 px-4">
        <div className="relative flex flex-col min-w-0 break-words w-full mb-6 shadow-lg rounded-lg bg-blueGray-100 border-0">
        <div className="rounded-t bg-white mb-0 px-6 py-6">
          <div className="text-center flex justify-between">
            <h6 className="text-blueGray-700 text-xl font-bold">จัดการสิทธิการใช้งาน</h6>
            <Link to="/admin/roles">
            <button
              className="bg-lightBlue-500 text-white active:bg-lightBlue-600 font-bold uppercase text-xs px-4 py-2 rounded shadow hover:shadow-md outline-none focus:outline-none mr-1 ease-linear transition-all duration-150"
              type="button"
            >
              รายการ
            </button>
            </Link>
          </div>
        </div>
        <div className="flex-auto px-4 lg:px-10 py-10 pt-10">
       
            
            <div className="flex flex-wrap">
              <div className="w-full lg:w-6/12 px-4">
                <div className="relative w-full mb-3">
                  <label
                    className="block uppercase text-blueGray-600 text-xs font-bold mb-2"
                    htmlFor="grid-password"
                  >
                    ชื่อสิทธิ : 
                  </label>
                  <input
                    type="text"
                    className="border-0 px-3 py-3 placeholder-blueGray-300 text-blueGray-600 bg-white rounded text-sm shadow focus:outline-none focus:ring w-full ease-linear transition-all duration-150"
                    defaultValue=""
                    onChange={(event)=>setName(event.target.value)} 
                  />
                </div>
              </div>
            </div>
            {/* <hr className="mt-2 mb-2 border-b-1 border-blueGray-300" /> */}
            <div className="mt-2 flex flex-wrap">
              <div className="w-full lg:w-6/12 px-4">
                <div className="relative w-full mb-3">
                <button class="bg-lightBlue-500 text-white active:bg-lightBlue-600 font-bold uppercase text-xs px-4 py-2 rounded shadow hover:shadow-md outline-none focus:outline-none mr-1 ease-linear transition-all duration-150" type="submit"
          onClick={()=>saveRole()}>บันทึก</button>
                </div>
              </div>
            </div>
        </div>
      </div>
        </div>
        
      </div>
      
    </>
  );
}
