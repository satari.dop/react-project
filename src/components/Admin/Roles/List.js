import React , { useEffect, useState } from "react";
import { confirmAlert } from 'react-confirm-alert'; // Import
import 'react-confirm-alert/src/react-confirm-alert.css'; // Import css

// services
import roleServices from "../../../services/Admin/Role"

import { Link } from "react-router-dom";
// components

export default function List() {
  const [ listRole, setListRole ] = useState([]);
  useEffect(()=>{

    async function fetchDataRole(){
      const res = await roleServices.listRole();
      setListRole(res.data);
    }

    fetchDataRole();
  },[]);
  
  const onClickDeleteConfirm = async (i,rol_id) => {
      confirmAlert({
        title: 'ยืนยันการการลบข้อมูล',
        //message: 'ต้องการลบข้อมูล.',
        buttons: [
          {
            label: 'ตกลง',
            onClick: () => onClickDelete(i,rol_id)
          },
          {
            label: 'ยกเลิก',
            onClick: () => setListRole(listRole)
          }
        ]
      });
  }

  const onClickDelete = async (i,rol_id) => {
      const res = await roleServices.delete(rol_id)
      if (res.success) {
        //alert(res.message) 
        setListRole(listRole.filter(item => item.rol_id !== rol_id));
      }
      else{
        alert(res.message);
      }
  }

  return (
    <>
      <div className="flex flex-wrap">
        <div className="w-full lg:w-12/12 px-4">
          <div className="relative flex flex-col min-w-0 break-words w-full mb-6 shadow-lg rounded-lg bg-blueGray-100 border-0">
            <div className="rounded-t bg-white mb-0 px-6 py-6">
              <div className="text-center flex justify-between">
                <h6 className="text-blueGray-700 text-xl font-bold">จัดการสิทธิการใช้งาน</h6>
                <Link to="/admin/roles/form">
                <button
                  className="bg-lightBlue-500 text-white active:bg-lightBlue-600 font-bold uppercase text-xs px-4 py-2 rounded shadow hover:shadow-md outline-none focus:outline-none mr-1 ease-linear transition-all duration-150"
                  type="button"
                >
                  เพิ่มรายการ
                </button>
                </Link>
              </div>
            </div>
            <div className="flex-auto px-4 lg:px-10 py-10 pt-10">
          
              <table className="items-center w-full bg-white border-collapse">
                <thead>
                  <tr>
                  <th
                      className={
                        "px-6 align-middle border border-solid py-3 text-xs uppercase border-l-0 border-r-0 whitespace-nowrap font-semibold text-left bg-blueGray-50 text-blueGray-500 border-blueGray-100" 
                      }
                    >
                      ลำดับ
                    </th>
                    <th
                      className={
                        "px-6 align-middle border border-solid py-3 text-xs uppercase border-l-0 border-r-0 whitespace-nowrap font-semibold text-left bg-blueGray-50 text-blueGray-500 border-blueGray-100" 
                      }
                    >
                      ชื่อสิทธิ
                    </th>
                    
                    <th
                      className={
                        "px-6 align-middle border border-solid py-3 text-xs uppercase border-l-0 border-r-0 whitespace-nowrap font-semibold text-left bg-blueGray-50 text-blueGray-500 border-blueGray-100"
                      }
                    >จัดการ</th>
                  </tr>
                </thead>
                <tbody>
                {
                  listRole.map((item,i)=>{
                    return(
                        <tr>
                          <td className="border-t-0 px-6 align-middle border-l-0 border-r-0 text-xs whitespace-nowrap p-4 text-center" width='5'>
                          {i+1}
                          </td>
                          <th className="border-t-0 px-6 align-middle border-l-0 border-r-0 text-xs whitespace-nowrap p-4 text-left flex items-center">
                            
                            <span
                              className={
                                " font-bold text-blueGray-600"
                              }
                            >
                              {item.rol_name}
                            </span>
                          </th>
                          
                          <td className="border-t-0 px-6 align-middle border-l-0 border-r-0 text-xs whitespace-nowrap p-4 text-left">
                          <Link to={"/admin/roles/edit/"+item.rol_id} class="bg-yellow-500 text-white active:bg-yellow-600 font-bold uppercase text-xs px-4 py-2 rounded shadow hover:shadow-md outline-none focus:outline-none mr-1 ease-linear transition-all duration-150"> แก้ไข </Link>
                          <a href="#" class="bg-red-500 text-white active:bg-red-600 font-bold uppercase text-xs px-4 py-2 rounded shadow hover:shadow-md outline-none focus:outline-none mr-1 ease-linear transition-all duration-150" onClick={()=>onClickDeleteConfirm(i,item.rol_id)}> ลบ </a>
                          </td>
                        </tr>
                      )
                    })
                  }
                </tbody>
              </table>
            
            </div>
          </div>
        </div>
      </div>
    </>
  );
}
