import axios from "axios";
const baseUrl = "http://localhost:8000/api/role"
const role = {};

role.save = async (data) => {
  const urlSave= baseUrl+"/create"
  const res = await axios.post(urlSave,data)
  .then(response=> {return response.data })
  .catch(error=>{ return error; })
  return res;
}

role.listRole = async () => {
  const urlList = baseUrl+"/list"
  const res = await axios.get(urlList)
  .then(response=>{ return response.data; })
  .catch(error=>{ return error; })
  return res;
}


role.get = async (rol_id) => {

  const urlGet = baseUrl+"/get/"+rol_id
  const res = await axios.get(urlGet)
  .then(response=>{ return response.data })
  .catch(error => { return error })
  return res;

}

role.update = async (data) => {
  const urlUpdate = baseUrl+"/update/"+data.rol_id
  const res = await axios.put(urlUpdate,data)
  .then(response=>{ return response.data; })
  .catch(error =>{ return error; })
  return res;
}

role.delete = async (rol_id) => {
  const urlDelete = baseUrl+"/delete/"+rol_id
  const res = await axios.delete(urlDelete)
  .then(response=> { return response.data })
  .catch(error =>{ return error })
  return res;
}
  
export default role
